## Movie app

В течении следующих дней вам предстоит реализовать простое приложение с использованием Typescript. Приложение должно быть написано на чистом Typescript без использования каких либо фреймворков.
Вам необходимо реализовать приложение с фильмами используя готовый API (MovieDB). Все данные на странице должны отображаться с помощью DOM API. Также у вас будет стартер приложения, с уже настроенным Typescript.